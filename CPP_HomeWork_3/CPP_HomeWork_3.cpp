#include <iostream>
#include <stdint.h>
#include <iomanip>
#include <string>

int main()
{
	std::cout << "Enter a random word or phrase: ";
	std::string word_or_phrase;
	std::getline(std::cin, word_or_phrase);

	std::cout <<"You wrote: "<< word_or_phrase <<"\n"<<"There are " 
	<<word_or_phrase.length() << " symbols" <<" "<<"\n";
	
	char firstname_initial;
	firstname_initial = word_or_phrase[0];

	char lastname_initial;
	lastname_initial = word_or_phrase[word_or_phrase.length() - 1];

	std::cout << "The first symbol of what you wrote is: " <<firstname_initial<<"\n";
	std::cout << "The last symbol of what you wrote is: " << lastname_initial << "\n";

	std::cin;
	return 0;

}